# sgart.dev Backend

---

### About

This is the backend of the Website located at sgart.dev.

---

### Selfhosting

Feel free to host this yourself. 
You are limited by using Mailgun as your email provider and Cloudflare Turnstile for captchas, which can be configured via configuration in the .env file.

The code is as is. Keep in mind, there might be security issues and bugs. I am not responsible for any damages caused by hosting this project yourself.

---

### Latest changes (2024-05-28)

- Add README.md
- Add LICENCE
- Add CHANGELOG
- Fix code analysis issues
- Add simple testing in Gitlab CI

---

### Roadmap
- [ ] Implement csrf middleware
- [ ] Implement user accounts
  - [ ] Implement Prisma database
  - [ ] Implement endpoints
