module.exports = {
  root: true,
  parser: '@typescript-eslint/parser',
  plugins: ['@typescript-eslint'],
  extends: ['eslint:recommended', 'plugin:@typescript-eslint/recommended'],
  parserOptions: {
    ecmaVersion: 2018,
    sourceType: 'module',
  },
  rules: {
    'quotes': ['error', 'single', { 'avoidEscape': true, 'allowTemplateLiterals': true }],
    'eol-last': ['error', 'always'],
    'no-restricted-imports': ['error', {
      'paths': [
        {
          name: 'node:process',
          message: 'Import of node:process is not needed and should be removed.',
        },
      ],
    }],
  },
};
