import {FormController} from '@controllers';
import { Router } from 'express';


export const contactRequestRoutes = Router();
contactRequestRoutes.post('/submit', FormController.submitContactRequest);
