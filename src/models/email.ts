export interface EmailModel {
  sender: string;
  receivers: string[];
  subject: string;
  body: {
    template?: {
      name: string;
      variables: object;
    };
    text?: string;
  },
  replyTo?: string;
  tag?: string;
}

export interface MailgunRequestModel {
  from: string;
  to: string[];
  cc?: string[];
  bcc?: string[];
  subject: string;
  text: string;
  template?: string;
  'h:X-Mailgun-Variables'?: string;
  'h:Reply-To'?: string;
  'o:Tag'?: string;
}
