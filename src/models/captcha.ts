export interface Captcha {
  token: string;
  ip?: string;
}
