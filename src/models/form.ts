import {Captcha} from '@models';

export interface ContactFormRequest {
  uuid: string;
  name: string;
  email: string;
  message: string;
  captcha: Captcha;
}
