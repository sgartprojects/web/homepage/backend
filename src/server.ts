import app from './app';
import {Logger} from '@helpers';

// Start server
app.listen(process.env.PORT, () => {
  return Logger.log(1,`App listening on port ${process.env.PORT}`);
})
