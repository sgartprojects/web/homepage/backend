import {EmailModel} from '@models';
import {EmailService} from '@services';
import {MailgunRequestModelConverter} from '../converters/mailgunRequestModelConverter';


export class EmailController {
  public static async sendEmail(emailData: EmailModel): Promise<boolean> {
    const mailgunRequestModel = MailgunRequestModelConverter.convert(emailData);
    return await EmailService.sendEmail(mailgunRequestModel);
  }
}
