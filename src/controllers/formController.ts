import {Request, Response} from 'express';
import {ContactFormRequest} from '@models';
import {ContactRequestValidator} from '@validators';
import {EmailController} from './emailController';
import {v4 as uuid} from 'uuid';


export class FormController {
  public static async submitContactRequest (req: Request, res: Response) {
    const contactRequest: ContactFormRequest = {
      uuid: uuid(),
      name: req.body.name.trim(),
      email: req.body.email.trim(),
      message: req.body.message.trim(),
      captcha: {
        token: req.body['cf-turnstile-token'],
        ip: req.header('CF-Connecting-IP')
      },
    }

    if (!await ContactRequestValidator.validate(contactRequest)) {
      res.status(400).send('Validation of request failed');
      return;
    }

    await EmailController.sendEmail({
      sender: 'Homepage <homepage@mg.sgart.dev>',
      receivers: [process.env.CONTACT_FORM_RECEIVER as string],
      subject: 'Contact Request',
      body: {
        template: {
          name: 'contact request',
          variables: {
            'name': contactRequest.name,
            'origin': 'sgart.dev',
            'message': contactRequest.message,
            'uuid': contactRequest.uuid,
          },
        },
      },
      replyTo: contactRequest.email,
      tag: 'ContactFormRequest-Request'
    });

    await EmailController.sendEmail({
      sender: 'sgart.dev NoReply <no-reply@mg.sgart.dev>',
      receivers: [contactRequest.email],
      subject: 'Contact Form Submission',
      body: {
        template: {
          name: 'no-reply contact request confirmation',
          variables: {'name': contactRequest.name},
        },
      },
      tag: 'ContactFormRequest-Confirmation'
    })

    res.status(200).send('On it!');
  }
}
