import {Logger} from '@helpers';
import Mailgun from 'mailgun.js';
import {MailgunRequestModel} from '@models';
import dotenv from 'dotenv';

// Load config to prevent error on access below
dotenv.config();

// eslint-disable-next-line @typescript-eslint/no-var-requires
const client = new Mailgun(require('form-data')).client({
  url: 'https://api.eu.mailgun.net',
  username: 'api',
  key: process.env.MG_API_KEY as string
})

export class EmailService {
  public static async sendEmail(data: MailgunRequestModel): Promise<boolean> {
    if (!parseInt(process.env.EMAIL_ENABLED as string)) {
      Logger.log(1, `Sending emails is disabled. Skipping email to ${data.to}`);
      return true;
    }

    const emailLimitedTo = process.env.EMAIL_LIMITED_TO?.split(' ');
    if (emailLimitedTo && emailLimitedTo.length > 0 && emailLimitedTo[0] !== '') {
      data.to = data.to.map((value) => {
        if (emailLimitedTo.indexOf(value) > -1) {
          return value;
        } else {
          return process.env.EMAIL_REPLACED_TO as string;
        }
      });
    }

    try {
      const res = await client.messages.create(process.env.MG_DOMAIN as string, data);
      Logger.log(0, res.message || '');
      return true;
    } catch (err) {
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      Logger.log(3, err.message)
      return false;
    }
  }
}
