import {EmailModel, MailgunRequestModel} from '@models';

export class MailgunRequestModelConverter {
  public static convert(source: EmailModel): MailgunRequestModel {
    return {
      from: source.sender,
      to: source.receivers,
      subject: source.subject,
      text: source.body.text || 'This email was sent without text. If you think this was a mistake, please try again.',
      template: source.body.template?.name,
      'h:X-Mailgun-Variables': JSON.stringify(source.body.template?.variables),
      'h:Reply-To': source.replyTo,
      'o:Tag': source.tag
    };
  }
}
