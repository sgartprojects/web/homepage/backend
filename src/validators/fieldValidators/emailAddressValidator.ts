const regex: RegExp = /^[A-Za-z0-9_\-.]{1,128}@[A-Za-z0-9_\-.]{1,128}\.[A-Za-z0-9_\-.]{1,128}/

export class EmailAddressValidator {
  public static validate(email: string): boolean {
    return regex.test(email);
  }
}
