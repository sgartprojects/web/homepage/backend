import {Captcha} from '@models';
import axios from 'axios';

export class CaptchaValidator {
  public static async validate(captcha: Captcha, uuid: string): Promise<boolean> {
    const formData = new FormData();
    formData.append('secret', process.env.CF_TURNSTILE_SECRET_KEY as string);
    formData.append('response', captcha.token);
    formData.append('idempotency_key', uuid)
    if (captcha.ip)
      formData.append('remoteip', captcha.ip);

    const res = await axios.post(process.env.CF_TURNSTILE_ENDPOINT as string, formData);
    return res.data.success || false;
  }
}
