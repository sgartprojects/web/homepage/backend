import {CaptchaValidator, EmailAddressValidator} from '@validators';
import {ContactFormRequest} from '@models';
import {Logger} from '@helpers';

export class ContactRequestValidator {
  public static async validate(request: ContactFormRequest): Promise<boolean> {
    if (request.name.trim().length == 0) {
      Logger.log(0, 'Validation of contactFormRequest failed: Name is empty')
      return false;
    }
    if (!EmailAddressValidator.validate(request.email)) {
      return false;
    }
    if (request.message.length > 2000) {
      return false;
    }
    if (!await CaptchaValidator.validate(request.captcha, request.uuid)) {
      return false;
    }
    return true;
  }
}
