// Module alias setup
import dotenv from 'dotenv';
dotenv.config();
if (process.env.NODE_ENV === 'Prod') {
  require('module-alias/register');
}
import express, { Application, NextFunction, Request, Response } from 'express';
import {contactRequestRoutes} from '@routes';
import helmet from 'helmet';
import bodyParser from 'body-parser';

const app: Application = express();

// configure and use cors
const cors = (req: Request, res: Response, next: NextFunction) => {
  res.setHeader('Access-Control-Allow-Origin', process.env.ALLOWED_ORIGINS as string);
  res.setHeader('Access-Control-Allow-Methods', process.env.ALLOWED_METHODS as string);
  res.setHeader('Access-Control-Allow-Headers', process.env.ALLOWED_HEADERS as string);
  res.setHeader('Access-Control-Allow-Credentials', process.env.ALLOW_CREDENTIALS as string);

  // Handle preflight requests
  if (req.method === 'OPTIONS') {
      // Respond with 200 for preflight requests
      res.sendStatus(200);
  } else {
      // Continue to the next middleware
      next();
  }
};
app.use(cors);

// Body parser middleware
app.use(bodyParser.json());

// Helmet middleware
app.use(helmet());

// Hide x-powered-by header
app.set('x-powered-by', false)

// register routes
app.use('/contactForm', contactRequestRoutes);

export default app;
