export class Logger {
   public static log = (code: number, message: string) => {
    // Check log level
    if (code < parseInt(process.env.LOG_LEVEL as string)) {
      return;
    }

    // Get time and create timestamp string
    const now = new Date();
    const currentTimestamp = `[${String(now.getFullYear()).padStart(2, '0')}-${String(now.getMonth() + 1).padStart(2, '0')}-${String(now.getDay()).padStart(2, '0')}] [${String(now.getHours()).padStart(2, '0')}:${String(now.getMinutes()).padStart(2, '0')}:${String(now.getSeconds()).padStart(2, '0')}]`;

    // Create barebone log message to append to
    let logMessage = `${currentTimestamp}`;

    // Append type to log message
    switch(code) {
      // Debug
      case 0:
        logMessage += ' [Debug   ]';
        break;
      // Info
      case 1:
        logMessage += ' [Info    ]';
        break;
      // Critical
      case 2:
        logMessage += ' [Critical]';
        break;
      // Error
      case 3:
        logMessage += ' [Error  ]';
        break;
      default:
        Logger.log(3, `Unable to log "${message}": Unknown log code "${code}"`);
        return;
    }

    // Append message to log message
    logMessage += ` ${message}`;

    // Print log message
    switch(code) {
      // Debug
      case 0:
        console.debug(logMessage);
        break;
      // Info
      case 1:
        console.info(logMessage);
        break;
      // Critical
      case 2:
        console.warn(logMessage);
        break;
      // Error
      case 3:
        console.error(logMessage);
        break;
      default:
        Logger.log(3, `Unable to log "${message}": Unknown log code "${code}"`);
        return;
    }
  }
}
